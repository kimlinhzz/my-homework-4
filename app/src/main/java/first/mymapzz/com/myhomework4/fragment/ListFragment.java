package first.mymapzz.com.myhomework4.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import first.mymapzz.com.myhomework4.OnItemClickListener;
import first.mymapzz.com.myhomework4.R;
import first.mymapzz.com.myhomework4.adapter.DataListAdapter;
import first.mymapzz.com.myhomework4.model.Email;

public class ListFragment extends Fragment {
    RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_list , container , false);
       recyclerView = view.findViewById(R.id.rcy_list);
       return view ;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DataListAdapter dataListAdapter = new DataListAdapter(Email.getData(), new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView email = view.findViewById(R.id.text_email);
                Toast.makeText(getContext() , "Data has been click at :" + email.getText() , Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(dataListAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
    }
}

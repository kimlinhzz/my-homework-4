package first.mymapzz.com.myhomework4;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view , int position);
 }

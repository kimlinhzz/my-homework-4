package first.mymapzz.com.myhomework4.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import first.mymapzz.com.myhomework4.OnItemClickListener;
import first.mymapzz.com.myhomework4.R;
import first.mymapzz.com.myhomework4.model.Email;

public class DataListAdapter extends RecyclerView.Adapter<DataListAdapter.ListViewHolder> {

    ArrayList<Email> emails;
    OnItemClickListener onItemClickListener;

    public DataListAdapter(ArrayList<Email> emails, OnItemClickListener onItemClickListener) {
        this.emails = emails;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        Email email = emails.get(position);
        TextView textEmail = holder.textView;
        textEmail.setText(email.getEmail());
    }

    @Override
    public int getItemCount() {
        return emails.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_email);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(view,getAdapterPosition());
        }
    }
}

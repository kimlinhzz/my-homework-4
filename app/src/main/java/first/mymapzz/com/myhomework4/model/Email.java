package first.mymapzz.com.myhomework4.model;

import java.util.ArrayList;

public class Email {

    String email;

    public Email(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static ArrayList<Email> getData() {
        ArrayList<Email> emails = new ArrayList<>();
        for (int i=0 ; i<40 ; i++){
            emails.add(new Email("lovkimlinh" + i + "@gmail.com"));
        }
        return emails;
    }
}

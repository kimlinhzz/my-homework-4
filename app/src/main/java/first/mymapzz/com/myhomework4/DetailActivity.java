package first.mymapzz.com.myhomework4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import first.mymapzz.com.myhomework4.adapter.DataListAdapter;
import first.mymapzz.com.myhomework4.model.Email;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    DataListAdapter dataListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        recyclerView = findViewById(R.id.rcy_list);
        dataListAdapter = new DataListAdapter(Email.getData(), new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView email = view.findViewById(R.id.text_email);
                Toast.makeText(getApplicationContext() , "Data has been click at :" + email.getText() , Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(dataListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

    }
}

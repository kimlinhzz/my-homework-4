package first.mymapzz.com.myhomework4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import first.mymapzz.com.myhomework4.fragment.ListFragment;
import first.mymapzz.com.myhomework4.fragment.MainFragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertical_main);
        MainFragment mainFragment = new MainFragment();
        ListFragment listFragment = new ListFragment();
        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
            replaceFragmentPortrait(mainFragment);
        }
        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            replaceFragmentLandscape(mainFragment, listFragment);
        }
    }

    private void replaceFragmentPortrait(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.free_space_1, fragment).commit();
    }

    private void replaceFragmentLandscape(Fragment fragment1, Fragment fragment2) {
        getSupportFragmentManager().beginTransaction().replace(R.id.free_space_1, fragment1).replace(R.id.free_space_2, fragment2).commit();
    }

}
